from django.shortcuts import render, redirect
from intuitlib.client import AuthClient 
from intuitlib.enums import Scopes
from django.conf import settings
from django.http import HttpResponse


QUICKBOOKS_CONFIG = settings.QUICKBOOKS
auth_client = AuthClient(**QUICKBOOKS_CONFIG)

# Create your views here.

def quickbooks_oauth(request):
    # scopes = [ 
    #     Scopes.ACCOUNTING, 
    # ]
    # auth_url = auth_client.get_authorization_url(scopes)

    auth_client.refresh(refresh_token=settings.REFRESH_TOKEN)
    import pdb;pdb.set_trace()
    return redirect('/admin')
    # return redirect(auth_url)

def quickbooks_redirect(request):
    auth_client.get_bearer_token(request.GET['code'], realm_id=request.GET['realmId'])
    # auth_client.access_token
    # auth_client.refresh_token
    return redirect('/admin')
