from intuitlib.client import AuthClient 
from intuitlib.enums import Scopes
from django.conf import settings
from django.http import HttpResponse


QUICKBOOKS_CONFIG = settings.QUICKBOOKS
auth_client = AuthClient(**QUICKBOOKS_CONFIG)


def get_quickbooks_access_token():
    auth_client.refresh(refresh_token=settings.REFRESH_TOKEN)
    return auth_client.access_token
    