from enum import Enum, IntEnum


class BaseEnum(Enum):
    """
    This is the base class for Enum Type
    It will define default functionality so that every Enum class does
    not need to redefine those
    """
    @classmethod
    def to_tuple(cls):
        return [(e.value, e.name) for e in cls]


class AccountTypeEnum(BaseEnum):
    """
    Define account types
    """

    ACCOUNTS_PAYABLE = "Accounts Payable"
    ACCOUNTS_RECEIVABLE = "Accounts Receivable"
    BANK = "Bank"
    CREDIT_CARD = "Credit Card"
    EQUITY = "Equity"
    CURRENT_ASSET = "Current Asset"
    FIXED_ASSET = "Fixed Asset"
    OTHER_CURRENT_ASSET = "Other Current Asset"
    OTHER_ASSET = "Other Asset"
    LONG_TERM_LIABILITY = "Long Term Liability"
    CURRENT_LIABILITY = "Current Liability"