from django.db import models
from django.db.models.signals import post_save
from .utils import get_quickbooks_access_token
import requests, json
from .enums import AccountTypeEnum
from django.conf import settings
# Create your models here.

class Account (models.Model):
    account_id = models.IntegerField()
    name = models.CharField(max_length=50)
    account_type = models.CharField(max_length = 255, choices=AccountTypeEnum.to_tuple())

    def __str__(self):
        return self.name


def save_account(sender, instance, created, **kwargs):
    if not created:
        return

    token = get_quickbooks_access_token()
    # print('Token = ', token)
    headers = {'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer ' + token }
    
    data = {
        "Name": instance.name, 
        "AccountType": instance.account_type
    }
    # print (data)
    url = '{0}/v3/company/{1}/account?minorversion=47'.format(settings.QUICKBOOKS_SANDBOX_URL, settings.REALMID)
    # print('Url ', url)
    res = requests.post(url, data =json.dumps(data), headers=headers)

    print(res.text)

post_save.connect(save_account, sender=Account)
